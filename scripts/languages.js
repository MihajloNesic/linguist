var $list = $("#languages");

var glLanguagesYML = "https://glcdn.githack.com/gitlab-org/linguist/raw/master/lib/linguist/languages.yml"

$.get(glLanguagesYML, function (data) {
    var languages = jsyaml.safeLoad(data);
    for (var language in languages) {
        attrs = languages[language];
        $list.append('<li style="border-bottom-color:' + attrs.color + '">' + language + '</li>');
    }
});